import argparse
from aiohttp import web
from aiopg.sa import create_engine

from aio_rotator.partner import PartnerView, PartnerRedirectView


HOST = "127.0.0.1"

parser = argparse.ArgumentParser(description="aiohttp server example")
parser.add_argument('--path')
parser.add_argument('--port')


async def init_pg(app):
    engine = await create_engine(
        database='rotator_db',
        user='n1kolyan',
        password='Kolvzad13665',
        host='localhost',
    )
    app['db'] = engine

if __name__ == "__main__":
    args = parser.parse_args()
    web_app = web.Application()
    web_app.add_routes([
        web.get('/partners/{name}/', PartnerView),
        web.get('/partners/partner_redirect/{partner_id}', PartnerRedirectView)
    ])
    web_app.on_startup.append(init_pg)
    web.run_app(web_app, host=HOST, port=args.port)
