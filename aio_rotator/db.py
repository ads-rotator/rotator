from sqlalchemy import (
    MetaData, Table, Column, Integer, String,
)

meta = MetaData()

PartnerCategory = Table(
    'partners_category', meta,

    Column('id', Integer, primary_key=True),
    Column('script', String, nullable=False),
    Column('slug_name', String, nullable=False)
)


Partner = Table(
    'partners_partnerprogram', meta,

    Column('id', Integer, primary_key=True),
    Column('link', String, nullable=False),
    Column('number_of_views', Integer),
    Column('category_id', Integer),
)
