from aiohttp import web

from aio_rotator.db import PartnerCategory, Partner


class PartnerView(web.View):
    async def get(self):
        name = self.request.match_info["name"]
        async with self.request.app["db"].acquire() as conn:
            partner = await conn.execute(
                PartnerCategory.select().where(PartnerCategory.c.slug_name == name)
            )
            partner = await partner.fetchone()
            await conn.execute(Partner.update().where(Partner.c.category_id==partner.id).values(number_of_views=Partner.c.number_of_views + 1))
        return web.Response(text=partner["script"])


class PartnerRedirectView(web.View):
    async def get(self):
        partner_id = self.request.match_info["partner_id"]
        async with self.request.app["db"].acquire() as conn:
            partner = await conn.execute(
                Partner.select().where(Partner.c.id == partner_id)
            )
            partner = await partner.fetchone()
        header = dict(location=partner["link"])
        raise web.HTTPFound(location=partner["link"], headers=header)
