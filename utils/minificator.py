import os
import subprocess
from django.conf import settings


class Minificator:
    OBFUSCATOR_PATH = 'node_modules/javascript-obfuscator/bin/javascript-obfuscator'
    OUTPUT_PATH = f'{settings.ROOT_DIR}/partners/templates/script.js'

    def __init__(self, template: str):
        self._template = template
        self.path_to_file = self.OUTPUT_PATH

    def _obfuscate(self) -> str:
        data = ''
        with open(self.path_to_file, 'wt', encoding='utf-8') as f:
            f.write(self._template)
        cmd = f'{self.OBFUSCATOR_PATH} {self.path_to_file} --string-array-encoding rc4 --output {self.path_to_file}'.split()
        output = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        stdout, stderr = output.communicate()
        if stderr is None:
            with open(self.path_to_file, 'rt') as f:
                data = f.read()
            os.remove(self.path_to_file)
        return data

    def obfuscate(self):
        minified = self._obfuscate()
        return minified
