# Download datasets
[Datasets](https://dev.maxmind.com/geoip/geoip2/geolite2/)

# Install libmaxminddb library

The libmaxminddb library provides a C library for reading MaxMind DB files,
including the GeoIP2 databases from MaxMind. This is a custom binary format
designed to facilitate fast lookups of IP addresses while allowing for great
flexibility in the type of data associated with an address.

## Install On Ubuntu via PPA

MaxMind provides a PPA for recent version of Ubuntu. To add the PPA to your
APT sources, run:

    $ sudo add-apt-repository ppa:maxmind/ppa

Then install the packages by running:

    $ sudo apt update
    $ sudo apt install libmaxminddb0 libmaxminddb-dev mmdb-bin
    
   
BUTT