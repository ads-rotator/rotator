import base64
from io import BytesIO
from PIL import Image
from ipware import get_client_ip
from django.contrib.gis.geoip2 import GeoIP2
from django.conf import settings
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.db.models import F
from django.db.models.fields.files import ImageFieldFile
from django.template.loader import render_to_string
from django.urls import reverse

from partners.models import PartnerProgram, Category
from utils.minificator import Minificator


class ImageGenerator:
    def __init__(self, partner_image):
        self.partner_image = partner_image
        self.default_image = ''

    def execute(self):
        encoded_image = None
        if self.partner_image is not None and self.partner_image.name != '':
            encoded_image = self.__decode_partner_image()
        # else:
        #     encoded_image = render_to_string('default_image.tpl')

        return encoded_image

    def __decode_partner_image(self):
        with open(self.partner_image.file.name, "rb") as image_file:
            image_base = base64.b64encode(image_file.read()).decode('UTF-8')
        return image_base


class ImageCropService:

    def __init__(self, partner):
        self.partner = partner

    def execute(self):
        if self.partner.image:
            self.partner.base_64_image = self._create_thumbnail(self.partner.image, self.partner.image_crop)

    def _create_thumbnail(self, image_field: ImageFieldFile, thumbnail_image_field: ImageFieldFile) -> str:
        image = Image.open(image_field.file.file)
        size = self._get_thumb_size(image.size[0], image.size[1])
        image.thumbnail(size=size)
        image_file = BytesIO()
        image.save(image_file, image.format)
        thumbnail_image_field.save(
            image_field.name,
            InMemoryUploadedFile(
                image_file,
                None, '',
                None,
                image.size,
                None,
            ),
            save=False
        )
        return ImageGenerator(thumbnail_image_field).execute()

    def _get_thumb_size(self, image_width, image_height) -> tuple:
        if image_width > image_height and image_width > 300:
            thumb_width = 300
            thumb_height = (image_height * thumb_width) / image_width
        elif image_height > image_width and image_height > 300:
            thumb_height = 300
            thumb_width = (image_width * thumb_height) / image_height
        else:
            thumb_width, thumb_height = 300, 300
        return thumb_width, thumb_height


class JsonDecoder:

    def __init__(self, partner_programs):
        self.partner_programs = partner_programs

    def execute(self):
        host = settings.HOST
        array_string = render_to_string('json_array.tpl', context=dict(partners=self.partner_programs, host=host))
        # encoded_array = base64.b64encode(bytes(array_string, 'UTF-8'))
        return array_string


class GenerateScriptService:

    def __init__(self, partner):
        self.partner = partner
        self.additional_partner = None

    def with_additional_partner(self, additional_partner):
        self.additional_partner = additional_partner
        return self

    def execute(self):
        partners = list(PartnerProgram.objects.filter(
            category__slug_name=self.partner.category
        ))

        if self.additional_partner:
            partners.append(self.additional_partner)

        encoded_image = self.partner.base_64_image
        encoded_array = JsonDecoder(partners).execute()
        partner_redirect_url = reverse('partners:partner_redirect', kwargs=dict(partner_id=self.partner.pk))

        context = dict(
            partners=encoded_array,
            category=self.partner.category,
            encoded_image=encoded_image,
            encoded_partner_redirect_url=f'{settings.HOST}{partner_redirect_url}'
        )

        script = render_to_string(template_name='tmp.js', context=context)
        category = self.partner.category
        script = Minificator(script).obfuscate()
        category.script = script
        category.save()


class ResponseScriptService:

    def __init__(self, category: str, request):
        self.category_slug = category
        self._request = request

    def execute(self) -> str:
        script = ''
        if not self._request.user_agent.is_mobile:
            category = Category.objects.filter(slug_name=self.category_slug).first()
            if category is not None and category.is_verify_geo:
                if not UserGeoIPService(self._request).is_country_accepted():
                    script = ''
                else:
                    script = self._generate_script(category)
            elif category is not None:
                script = self._generate_script(category)
        return script

    def _generate_script(self, category):
        if category.script != '':
            script = category.script
            self.__update_number_of_views()
        else:
            script = self.__generate_script()
        return script

    def __generate_script(self) -> str:
        partners = self.__update_number_of_views()
        partner = partners.first()
        encoded_image = partner.base_64_image
        encoded_array = JsonDecoder(partners).execute()
        partner_redirect_url = reverse('partners:partner_redirect', kwargs=dict(partner_id=partners.first().pk))
        encoded_partner_redirect_url = base64.b64encode(
            bytes(f'{settings.HOST}{partner_redirect_url}', 'UTF-8')
        ).decode('UTF-8')
        context = dict(
            partners=encoded_array,
            category=self.category_slug,
            encoded_image=encoded_image,
            encoded_partner_redirect_url=encoded_partner_redirect_url
        )
        script = render_to_string(template_name='template.js', context=context)
        return script

    def __update_number_of_views(self):
        partners = PartnerProgram.objects.filter(
            category__slug_name=self.category_slug
        )
        # TODO: исправить операцию записи
        partners.update(number_of_views=F('number_of_views') + 1)
        return partners


class UserGeoIPService:
    COUNTRIES = ('Slovenia', 'Republic of Lithuania', 'Estonia', 'Croatia', 'Latvia', 'Brunei', 'Serbia', 'Bulgaria',
                 'Czechia', 'Slovakia', 'Finland', 'Montenegro', 'Israel', 'South Korea', 'Belgium', 'Hungary',
                 'Austria', 'Belarus', 'Switzerland', 'Spain', 'Denmark', 'Russia')

    def __init__(self, request):
        self._request = request
        self.geo_ip = GeoIP2()

    def is_country_accepted(self) -> bool:
        is_accepted = False
        user_ip, is_routable = get_client_ip(self._request)
        if user_ip is not None and is_routable:
            country_dict = self.geo_ip.country(user_ip)
            country = country_dict.get('country_name', '')
            if country in self.COUNTRIES:
                is_accepted = True
        return is_accepted
