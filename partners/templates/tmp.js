'use strict';

(function () {
    let link_list = {{ partners }};
    let s  = document.currentScript;
    s.before(createLnk(link_list));

    function createLnk(links) {
        let lnk = document.createElement('a');
        lnk.href = '{{ encoded_partner_redirect_url }}';
        lnk.target = '_blank';
        lnk.prepend(createImg());
        return lnk;
    }

    function createImg() {
        var img = '';
        if ('{{ encoded_image }}' != '') {
            img = document.createElement('img');
            img.id = '123';
            img.src = 'data:image/png;base64, {{ encoded_image }}';
        }
        return img;
    }

    function createIframe(linkItem) {
        let ifr = document.createElement('iframe');
        ifr.src = linkItem['link'];
        ifr.id = linkItem['id'];
        ifr.height = '0';
        ifr.width = '0';
        ifr.style.display = 'none';
        return ifr;
    }

    function add(linkData) {
        /* зачем эта функция когда она линейнная и можно весь код держать
        / в анонимоной функции цикла?
         */
        let old = document.getElementById(linkData['id']);
        if (old) old.remove();
        document.body.append(createIframe(linkData));
    }
    function rotate(linksArray) {
        linksArray.forEach(function (element) {
            add(element);
        });
    }

    document.addEventListener('DOMContentLoaded', execute);

    async function execute() {
        // вообще не понятно надо подумать как упростить
        rotate(link_list);
        setTimeout(link_list.forEach(function (element) {
                setInterval(function () {
                        add(element)
                    },
                    element['freq'] * 1000
                )
            }),
            1000
        )
    }
})();
