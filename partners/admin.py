from django.contrib import admin

from partners.models import Category, PartnerProgram, AdditionalLink
from partners.service import GenerateScriptService, ImageGenerator


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug_name', 'created_at']
    prepopulated_fields = {'slug_name': ('name',)}
    ordering = ('name',)
    search_fields = ('name',)


@admin.register(PartnerProgram)
class PartnerProgramAdmin(admin.ModelAdmin):
    list_display = ['name', 'link', 'number_of_views', 'category', 'frequency']
    search_fields = ('name',)
    readonly_fields = ('image_crop',)

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)

        if obj.image:
            obj.base_64_image = ImageGenerator(obj.image).execute()
            obj.save()
        partner = obj
        GenerateScriptService(partner).execute()


@admin.register(AdditionalLink)
class AdditionalLinkAdmin(admin.ModelAdmin):
    list_display = ['name', 'link', 'frequency']
    search_fields = ('name',)

    def save_model(self, request, obj, form, change):

        super().save_model(request, obj, form, change)
        if obj.is_active:
            self.__rebuild_category(obj)
        else:
            self.__rebuild_category()

    def delete_model(self, request, obj):
        super().delete_model(request, obj)
        if obj.is_active():
            self.__rebuild_category(obj)
        else:
            self.__rebuild_category()

    def __rebuild_category(self, obj=None):
        category_list = Category.objects.all()
        for category in category_list:
            partner = category.partners.all().first()
            if partner is not None:
                service = GenerateScriptService(partner)
                if obj and category.is_verify_geo is False:
                    service.with_additional_partner(obj)
                service.execute()
