from django.http import HttpResponse
from django.urls import reverse
from django.views.generic.base import View, RedirectView, TemplateView

from partners.models import PartnerProgram, AdditionalLink
from partners.service import ResponseScriptService


class PartnerTeapotPageView(TemplateView):

    template_name = 'teapot.html'


class PartnerProgramGenerateScriptView(View):

    def get(self, request, **kwargs):
        category_slug_name = kwargs.get('slug_name')
        script = ResponseScriptService(category_slug_name, request).execute()
        return HttpResponse(script)


class PartnerProgramRedirectView(RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        partner_id = kwargs.get('partner_id')
        additional_id = kwargs.get('additional')
        if additional_id:
            partner = AdditionalLink.objects.filter(pk=partner_id).first()
        else:
            partner = PartnerProgram.objects.filter(pk=partner_id).first()
        return partner.link if partner else reverse('partners:teapot')

