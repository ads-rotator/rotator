
from django.db import models
from user.models import User


class Category(models.Model):
    name = models.CharField(max_length=128, verbose_name='Название')
    slug_name = models.SlugField(max_length=128, verbose_name='СЛУГ')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    max_count = models.PositiveIntegerField(default=1, verbose_name='MAX количество ссылок')
    script = models.TextField(verbose_name='Скрипт', blank=True, null=True)
    user = models.ForeignKey(User, verbose_name='Владелец', blank=True, null=True, on_delete=models.SET_NULL)
    is_verify_geo = models.BooleanField(verbose_name='Проверять страну', default=False)

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name


class PartnerProgram(models.Model):
    name = models.CharField(max_length=500, verbose_name='Название')
    link = models.URLField(verbose_name='Ссылка на партнерскую программу')
    category = models.ForeignKey(Category, verbose_name='Категория',
                                 on_delete=models.SET_NULL, related_name='partners', null=True)
    number_of_views = models.PositiveIntegerField(default=0, verbose_name='Количество просмотров')
    image = models.ImageField(upload_to='media/orig', null=True, blank=True, verbose_name='Изображнение')
    image_crop = models.ImageField(upload_to='media/crop', null=True, blank=True, verbose_name='Обрезаное изображение')
    frequency = models.IntegerField(verbose_name='Частота', default=60)
    base_64_image = models.TextField(verbose_name='base64_image', blank=True, null=True)

    class Meta:
        verbose_name = 'Партнерская программа'
        verbose_name_plural = 'партнерские программы'

    def __str__(self):
        return self.name


class AdditionalLink(models.Model):
    name = models.CharField(max_length=500, verbose_name='Название')
    link = models.URLField(verbose_name='Ссылка на партнерскую программу')
    frequency = models.IntegerField(verbose_name='Частота', default=600)
    is_active = models.BooleanField(verbose_name='Активна', default=True)

    class Meta:
        verbose_name = 'Тестовая партнерская программа'
        verbose_name_plural = 'Тестовые партнерские программы'

    def __str__(self):
        return self.name
