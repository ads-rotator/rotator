# Generated by Django 2.2.4 on 2019-08-12 14:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('partners', '0003_auto_20190810_1345'),
    ]

    operations = [
        migrations.AddField(
            model_name='partnerprogram',
            name='number_of_views',
            field=models.PositiveIntegerField(default=0, verbose_name='Количество просмотров'),
        ),
    ]
