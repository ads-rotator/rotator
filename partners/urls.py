from django.urls import path

from partners.views import PartnerProgramGenerateScriptView, PartnerProgramRedirectView, PartnerTeapotPageView

app_name = 'partners'

urlpatterns = [
    path('', PartnerTeapotPageView.as_view(), name='teapot'),
    path('<slug_name>/', PartnerProgramGenerateScriptView.as_view(), name='generate_script'),
    path(
        'partner_redirect/<int:partner_id>/<int:additional>/',
        PartnerProgramRedirectView.as_view(),
        name='partner_redirect'
    ),
    path('partner_redirect/<int:partner_id>', PartnerProgramRedirectView.as_view(), name='partner_redirect'),
]
